# README #

**Unity _4.6.0_**

The application consists of a digital version of the story "Melancia - Coco Verde"​, which is part of the work "Contos Gaúchescos"​ of Simoes Lopes Neto (an author who narrates the formation of the people and state of Rio Grande do Sul). The purpose of this proposal is to bring the digital support the story that was written at the end of the nineteenth century and early twentieth century in order to approach the young people to this significant text of "Gaúchos" literature. It also proposes an aid tool for understanding, presenting a glossary with the meaning of terms regionalists and of ancient times; and is an accessibility tool, with the possibility to listen to the reading of the full text.

### Change Log ###

0.9 - Published in BETA.