﻿using System.Collections;
using UnityEngine;

public class ImageFullScreen : MonoBehaviour
{
	#region [ METHODS ]

	void Start()
	{
		RectTransform rectParent = GetComponent<RectTransform>();

		rectParent.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, Screen.width);
	}

	#endregion
}