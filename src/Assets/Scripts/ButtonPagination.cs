﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonPagination : MonoBehaviour
{
	#region [ FIELDS ]

	[SerializeField]
	private List<GameObject> pages = new List<GameObject>();

	/// <summary>
	/// The index of the page.
	/// </summary>
	private int pageIndex = 0;

	#endregion

	#region [ METHODS ]

	void Start()
	{
		Scrollbar scrollbar = GetComponentInChildren<Scrollbar>();
		scrollbar.SetDirection(Scrollbar.Direction.LeftToRight, false);
		scrollbar.value = 0;

		pageIndex = 0;

		pages.ForEach(p => p.SetActive(false));
		pages[pageIndex].SetActive(true);
	}

	public void PreviousPage(int nextPaginationIndex)
	{
		InactiveCurrent();
		pageIndex--;
		
		if (pageIndex >= 0) {
			GlobalVariables.Manager.PlayPaginationClip();

			pages[pageIndex].SetActive(true);
		}
		else {
			pageIndex++;
			pages[pageIndex].SetActive(true);

			GlobalVariables.Manager.NextPage(nextPaginationIndex);
		}
	}

	public void NextPage(int nextPaginationIndex)
	{
		InactiveCurrent();
		pageIndex++;

		if (pageIndex < pages.Count) {
			pages[pageIndex].SetActive(true);

			GlobalVariables.Manager.PlayPaginationClip();
		}
		else {
			pageIndex--;
			pages[pageIndex].SetActive(true);

			GlobalVariables.Manager.NextPage(nextPaginationIndex);
		}
	}

	public void SetLetterPage(int currentPageIndex)
	{
		GlobalVariables.Manager.PlayPaginationClip();
		
		InactiveCurrent();
		pageIndex = currentPageIndex;

		pages[pageIndex].SetActive(true);
	}

	private void InactiveCurrent()
	{
		if (pageIndex >= 0 && pageIndex < pages.Count) {
			pages[pageIndex].SetActive(false);
		}
	}

	#endregion
}