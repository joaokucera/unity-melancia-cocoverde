﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class CanvasHandler : MonoBehaviour 
{
	#region [ FIELDS ]

	private CanvasScaler scaler;

	#endregion

	#region [ METHODS ]

	void Start ()
	{
		scaler = GetComponent<CanvasScaler> ();

		scaler.uiScaleMode = CanvasScaler.ScaleMode.ScaleWithScreenSize;
	}

	#endregion
}