﻿using System.Collections;
using DG.Tweening;
using UnityEngine;

public class MenuControl : MonoBehaviour
{
	#region [ FIELDS ]

	[SerializeField] 
	private float xOffset = 48f;
	/// <summary>
	/// The open close clip.
	/// </summary>
	[SerializeField]
	private AudioClip openCloseClip;

	/// <summary>
	/// The is open.
	/// </summary>
	private bool isOpen = false;
	/// <summary>
	/// The was playback sound before.
	/// </summary>
	private bool wasPlaybackSoundBefore;
	/// <summary>
	/// The x position OUT.
	/// </summary>
	private float xPositionOUT;
	/// <summary>
	/// The x position IN.
	/// </summary>
	private float xPositionIN;

	#endregion

	#region [ METHODS ]

	void Awake()
	{
		xPositionIN = transform.localPosition.x;
		xPositionOUT = xPositionIN - (transform as RectTransform).sizeDelta.x + xOffset;

		transform.DOLocalMoveX(xPositionOUT, 0);
	}

	/// <summary>
	/// Raises the click event.
	/// </summary>
	public void OnClick()
	{
		PlayOpenCloseClip();

		isOpen = !isOpen;

		if (isOpen) {
			wasPlaybackSoundBefore = GlobalVariables.EnablePlaybackSound;

			transform.DOLocalMoveX(xPositionIN, 0.5f);

			GlobalVariables.Indice.Close(false);

			GlobalVariables.Manager.PlayOrPausePlaybackSound(VoiceAction.Pause);
		}
		else {
			transform.DOLocalMoveX(xPositionOUT, 0.5f);

			if (wasPlaybackSoundBefore) {
				GlobalVariables.Manager.PlayOrPausePlaybackSound(VoiceAction.Play);
			}
		}
	}

	/// <summary>
	/// Raises the click event.
	/// </summary>
	public void OnClickAndNextPage(int nextIndex)
	{
		Close();

		GlobalVariables.Manager.NextPageByMenu(nextIndex);
	}

	/// <summary>
	/// Plaies the open close clip.
	/// </summary>
	private void PlayOpenCloseClip()
	{
		if (GlobalVariables.EnableRingingSounds) {
			AudioSource.PlayClipAtPoint(openCloseClip, Vector3.zero);
		}
	}

	/// <summary>
	/// Tos the close.
	/// </summary>
	public void Close()
	{
		if (isOpen) {
			PlayOpenCloseClip();

			isOpen = false;

			transform.DOLocalMoveX(xPositionOUT, 0.5f);
		}
	}

	#endregion
}