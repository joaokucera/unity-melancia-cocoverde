﻿/// <summary>
/// Type setting.
/// </summary>
public enum TypeSetting
{
	RingingSounds,
	// SONS DE TOQUE
	PlaybackSound,
	// SOM DE LEITURA
	SoundEffects,
	// EFEITOS ESPECIAIS
	Soundtrack
	// TRILHA SONORA
}

/// <summary>
/// Sound control icon.
/// </summary>
public enum SoundControlIcon
{
	Play = 0,
	Pause = 1
}

/// <summary>
/// Voice action.
/// </summary>
public enum VoiceAction
{
	Play,
	Pause,
	Reverse
}

/// <summary>
/// Book page type.
/// </summary>
public enum BookPageType
{
	BookCover = 0,
	HowToUse_Page = 1,
    Tales = 2,
	Settings = 3,
	Glossary_Page = 4,
    Presentation = 5,
    Foreword_Page_1 = 6,
    Foreword_Page_2 = 7,
    Credits_Page_1 = 8,
    Credits_Page_2 = 9,

	Patricio_Blau_Cover = 10,
	Patricio_Blau_Page_1 = 11,
	Patricio_Blau_Page_2 = 12,

	Melancia_CocoVerde_Cover = 13,
	Melancia_CocoVerde_Page_1 = 14,
	Melancia_CocoVerde_Page_2_1 = 15,
	Melancia_CocoVerde_Page_2_2 = 16,
	Melancia_CocoVerde_Page_2_3 = 17,
	Melancia_CocoVerde_Page_3_1 = 18,
	Melancia_CocoVerde_Page_3_2 = 19,
	Melancia_CocoVerde_Page_4 = 20,
	Melancia_CocoVerde_Page_5_1 = 21,
	Melancia_CocoVerde_Page_5_2 = 22,
	Melancia_CocoVerde_Page_5_3 = 23,
	Melancia_CocoVerde_Page_6_1 = 24,
	Melancia_CocoVerde_Page_6_2 = 25,
	Melancia_CocoVerde_Page_7_1 = 26,
	Melancia_CocoVerde_Page_7_2 = 27,
	Melancia_CocoVerde_Page_7_3 = 28,
	Melancia_CocoVerde_Page_7_4 = 29,
	Melancia_CocoVerde_Page_7_5 = 30,
	Melancia_CocoVerde_Page_8_1 = 31,
	Melancia_CocoVerde_Page_8_2 = 32,
	Melancia_CocoVerde_Page_8_3 = 33,
	Melancia_CocoVerde_Page_9_1 = 34,
	Melancia_CocoVerde_Page_9_2 = 35,
	Melancia_CocoVerde_Page_10 = 36,
	Melancia_CocoVerde_Page_11 = 37,
	Melancia_CocoVerde_Page_12 = 38,
	Melancia_CocoVerde_Page_13 = 39,
	Melancia_CocoVerde_Page_14 = 40,
	Melancia_CocoVerde_Page_15 = 41,
	Melancia_CocoVerde_Page_16 = 42,
	Melancia_CocoVerde_Page_17 = 43,
	Melancia_CocoVerde_Page_18_1 = 44,
	Melancia_CocoVerde_Page_18_2 = 45,
	Melancia_CocoVerde_Page_19 = 46,
	Melancia_CocoVerde_Page_20_1 = 47,
	Melancia_CocoVerde_Page_20_2 = 48,
	Melancia_CocoVerde_Page_21 = 49,
	Melancia_CocoVerde_Page_22_1 = 50,
	Melancia_CocoVerde_Page_22_2 = 51,
	Melancia_CocoVerde_Page_23 = 52,
	Melancia_CocoVerde_Page_24_1 = 53,
	Melancia_CocoVerde_Page_24_2 = 54,
	Melancia_CocoVerde_Page_25 = 55,
	Melancia_CocoVerde_Page_26_1 = 56,
	Melancia_CocoVerde_Page_26_2 = 57,
	Melancia_CocoVerde_Page_26_3 = 58,
	Melancia_CocoVerde_Page_26_4 = 59,
	Melancia_CocoVerde_Page_26_5 = 60,
	Melancia_CocoVerde_Page_27_1 = 61,
	Melancia_CocoVerde_Page_27_2 = 62,
	Melancia_CocoVerde_Page_28_1 = 63,
	Melancia_CocoVerde_Page_28_2 = 64,
	Melancia_CocoVerde_Page_28_3 = 65,
	Melancia_CocoVerde_Page_29 = 66,
	Melancia_CocoVerde_Page_30 = 67,
	Melancia_CocoVerde_Page_31 = 68
}