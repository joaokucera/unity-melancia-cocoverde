using UnityEngine;

public class GlobalVariables : MonoBehaviour
{
	#region [ FIELDS ]

	/// <summary>
	/// The enable ringing sounds.
	/// </summary>
	private static bool enableRingingSounds = true;
	/// <summary>
	/// The enable playback sound.
	/// </summary>
	private static bool enablePlaybackSound = true;
	/// <summary>
	/// The enable sound effects.
	/// </summary>
	private static bool enableSoundEffects = true;
	/// <summary>
	/// The enable soundtrack.
	/// </summary>
	private static bool enableSoundtrack = true;
	/// <summary>
	/// The manager.
	/// </summary>
	private static ManagerPagination manager;
	/// <summary>
	/// The menu.
	/// </summary>
	private static MenuControl menu;
	/// <summary>
	/// The indice.
	/// </summary>
	private static IndiceControl indice;

	#endregion

	#region [ PROPERTIES ]

	/// <summary>
	/// Gets the manager.
	/// </summary>
	/// <value>The manager.</value>
	public static ManagerPagination Manager {
		get {
			if (ReferenceEquals(manager, null)) {
				manager = GameObject.FindObjectOfType<ManagerPagination>();
			}
			
			return manager;
		}
	}

	/// <summary>
	/// Gets the menu.
	/// </summary>
	/// <value>The menu.</value>
	public static MenuControl Menu {
		get {
			if (ReferenceEquals(menu, null)) {
				menu = GameObject.FindObjectOfType<MenuControl>();
			}
			
			return menu;
		}
	}

	/// <summary>
	/// Gets the indice.
	/// </summary>
	/// <value>The indice.</value>
	public static IndiceControl Indice {
		get {
			if (ReferenceEquals(indice, null)) {
				indice = GameObject.FindObjectOfType<IndiceControl>();
			}
			
			return indice;
		}
	}

	/// <summary>
	/// Gets or sets a value indicating whether this <see cref="GlobalVariables"/> is paused.
	/// </summary>
	/// <value><c>true</c> if paused; otherwise, <c>false</c>.</value>
	public static bool Paused {
		get {
			if (Time.timeScale == 0f) {
				return true;
			}
			else {
				return false;
			}
		}
		set {
			if (value) {
				Time.timeScale = 0f;
			}
			else {
				Time.timeScale = 1f;
			}
		}
	}

	/// <summary>
	/// SONS DE TOQUE (clicks, botões, etc),
	/// </summary>
	public static bool EnableRingingSounds {
		get {
			return enableRingingSounds;
		}
		set {
			enableRingingSounds = value;
		}
	}

	/// <summary>
	/// NARRAÇÃO (voice over do livro),
	/// </summary>
	public static bool EnablePlaybackSound {
		get {
			return enablePlaybackSound;
		}
		set {
			enablePlaybackSound = value;
		}
	}

	/// <summary>
	/// EFEITOS DE SOM (sons de fundo do livro),
	/// </summary>
	public static bool EnableSoundEffects {
		get {
			return enableSoundEffects;
		}
		set {
			enableSoundEffects = value;
		}
	}

	/// <summary>
	/// MÚSICA (fora do livro).
	/// </summary>
	public static bool EnableSoundtrack {
		get {
			return enableSoundtrack;
		}
		set {
			enableSoundtrack = value;
		}
	}

	#endregion
}