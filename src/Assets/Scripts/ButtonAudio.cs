﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class ButtonAudio : MonoBehaviour
{
	#region [ FIELDS ]

	private Text textUI;

	private string enabledText = "Desabilitar Áudio";
	private string disabledText = "Habilitar Áudio";

	private Color enabledColor = new Color(30f / 255f, 60f / 255f, 90f / 255f, 1f);
	private Color disabledColor = new Color(30f / 255f, 60f / 255f, 90f / 255f, 0.5f);

	#endregion

	#region [ METHODS ]

	void Start()
	{
		textUI = GetComponentInChildren<Text>();

		SetText();
	}

	public void EnabledAudio()
	{
		GlobalVariables.EnablePlaybackSound = !GlobalVariables.EnablePlaybackSound;

		SetText();
	}

	private void SetText()
	{
		if (GlobalVariables.EnablePlaybackSound) {
			textUI.text = enabledText;
			textUI.color = enabledColor;
		}
		else {
			textUI.text = disabledText;
			textUI.color = disabledColor;
		}
	}

	#endregion
}