﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class ParallaxAnimation : MonoBehaviour
{
	#region [ FIELDS ]

	[SerializeField]
	private float speed;
	[SerializeField]
	private bool randomPosition;

	private List<Transform> rects = new List<Transform>();

	#endregion

	#region [ METHODS ]

	void Start()
	{
		for (int i = 0; i < transform.childCount; i++) {
			Transform child = transform.GetChild(i);

			rects.Add(child);
		}

		rects = rects.OrderBy(r => r.position.x).ToList();
	}

	void FixedUpdate()
	{
		for (int i = 0; i < rects.Count; i++) {
			rects[i].Translate(-Vector3.right * speed * Time.deltaTime);
		}
	
		Transform firstChild = rects.FirstOrDefault();

		if (!ReferenceEquals(firstChild, null)) {
			if (firstChild.position.x < Camera.main.transform.position.x) {
				Renderer firstRenderer = firstChild.GetComponent<Renderer>();

				if (!firstRenderer.isVisible) {
					Transform lastChild = rects.LastOrDefault();
					Renderer lastRenderer = lastChild.GetComponent<Renderer>();

					Vector3 lastPosition = lastChild.position;
					Vector3 lastSize = lastRenderer.bounds.max - lastRenderer.bounds.min;

					if (randomPosition) {
						lastPosition.x += Random.Range(5, 20);
					}

					firstChild.position = new Vector3(lastPosition.x + lastSize.x - 0.25f, firstChild.position.y, firstChild.position.z);

					rects.Remove(firstChild);
					rects.Add(firstChild);
				}
			}
		}
	}

	#endregion
}