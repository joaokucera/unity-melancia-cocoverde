﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SpritePagination : MonoBehaviour
{
	#region [ FIELDS ]

	[SerializeField]
	private Image imageUI;
	[SerializeField]
	private List<Sprite> sprites = new List<Sprite>();
	private int pageIndex = 0;

	#endregion

	#region [ METHODS ]

	void Start()
	{
		pageIndex = 0;

		imageUI.sprite = sprites[pageIndex];
	}

	public void PreviousPage(int nextPaginationIndex)
	{
		GlobalVariables.Manager.PlayPaginationClip();

		pageIndex--;
		
		if (pageIndex >= 0) {
			imageUI.sprite = sprites[pageIndex];
		}
		else {
			pageIndex++;
			GlobalVariables.Manager.NextPage(nextPaginationIndex);
		}
	}

	public void NextPage(int nextPaginationIndex)
	{
		GlobalVariables.Manager.PlayPaginationClip();

		pageIndex++;

		if (pageIndex < sprites.Count) {
			imageUI.sprite = sprites[pageIndex];
		}
		else {
			pageIndex--;
			GlobalVariables.Manager.NextPage(nextPaginationIndex);
		}
	}

	#endregion
}