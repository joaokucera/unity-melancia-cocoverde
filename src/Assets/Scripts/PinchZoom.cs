﻿using UnityEngine;
using UnityEngine.UI;

public class PinchZoom : MonoBehaviour
{
    #region [ FIELDS ]

    /// <summary>
    /// The main camera.
    /// </summary>
    private CanvasScaler scaler;
    /// <summary>
    /// The orthographic zoom speed.
    /// </summary>
    private float zoomSpeed = 0.5f;

    #endregion

    #region [ METHODS ]

    void Start()
    {
        scaler = GetComponent<CanvasScaler>();
    }

    void Update()
    {
#if UNITY_EDITOR
		float scroll = Input.GetAxis("Mouse ScrollWheel");

		scaler.matchWidthOrHeight += scroll * zoomSpeed;
		scaler.matchWidthOrHeight = Mathf.Clamp(scaler.matchWidthOrHeight, 0, 1);
#else
        if (Input.touchCount == 2) {
            Touch touchZero = Input.GetTouch(0);
            Touch touchOne = Input.GetTouch(1);

            Vector2 touchZeroPrevPosition = touchZero.position - touchZero.deltaPosition;
            Vector2 touchOnePrevPosition = touchOne.position - touchOne.deltaPosition;

            float prevTouchDeltaMagnitude = (touchZeroPrevPosition - touchOnePrevPosition).magnitude;
            float touchDeltaMagnitude = (touchZero.position - touchOne.position).magnitude;

            float deltaMagnitudeDiff = prevTouchDeltaMagnitude - touchDeltaMagnitude;

            scaler.matchWidthOrHeight += deltaMagnitudeDiff * zoomSpeed;
            scaler.matchWidthOrHeight = Mathf.Max(scaler.matchWidthOrHeight, 1);
        }
#endif
    }

    #endregion
}