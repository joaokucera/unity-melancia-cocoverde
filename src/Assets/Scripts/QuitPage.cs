﻿using System.Collections;
using UnityEngine;

public class QuitPage : MonoBehaviour
{
	#region [ METHODS ]

	public void OnContinue()
	{
		gameObject.SetActive(false);
	}

	public void OnQuit()
	{
		Application.Quit();
	}

	#endregion
}