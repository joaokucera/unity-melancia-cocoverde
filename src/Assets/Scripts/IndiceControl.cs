﻿using System.Collections;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class IndiceControl : MonoBehaviour
{
	#region [ FIELDS ]

	/// <summary>
	/// The is open.
	/// </summary>
	private bool isOpen = false;
	/// <summary>
	/// The was playback sound before.
	/// </summary>
	private bool wasPlaybackSoundBefore;
	/// <summary>
	/// The x position OUT.
	/// </summary>
	private float xPositionOUT;
	/// <summary>
	/// The x position IN.
	/// </summary>
	private float xPositionIN;

	#endregion

	#region [ METHODS ]

	void Awake()
	{
		xPositionIN = transform.localPosition.x;
		xPositionOUT = xPositionIN + (transform as RectTransform).sizeDelta.x;

		transform.DOLocalMoveX(xPositionOUT, 0);
	}

	/// <summary>
	/// Start is called on the frame when a script is enabled just before any of the Update methods is called the first time.
	/// </summary>
	void Start()
	{
		Scrollbar scrollbar = GetComponentInChildren<Scrollbar>();
		scrollbar.SetDirection(Scrollbar.Direction.BottomToTop, false);
		scrollbar.value = 1;
	}

	/// <summary>
	/// Open this instance.
	/// </summary>
	public void Open(bool lookPlayback = true)
	{
		if (!isOpen) {
			wasPlaybackSoundBefore = GlobalVariables.EnablePlaybackSound;

			isOpen = true;

			transform.DOLocalMoveX(xPositionIN, 0.5f);

			if (lookPlayback) {
				GlobalVariables.Manager.PlayOrPausePlaybackSound(VoiceAction.Pause);
			}

			GlobalVariables.Menu.Close();
		}
	}

	/// <summary>
	/// Close this instance.
	/// </summary>
	public void Close(bool lookPlayback = true)
	{
		if (isOpen) {
			isOpen = false;
		
			transform.DOLocalMoveX(xPositionOUT, 0.5f);

			if (lookPlayback && wasPlaybackSoundBefore) {
				GlobalVariables.Manager.PlayOrPausePlaybackSound(VoiceAction.Play);
			}
		}
	}

	#endregion
}