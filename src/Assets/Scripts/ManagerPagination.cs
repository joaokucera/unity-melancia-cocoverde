﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(AudioSource))]
public class ManagerPagination : MonoBehaviour
{
    #region [ FIELDS ]

    [SerializeField]
    private AudioClip soundtrackClip;
    [SerializeField]
    private AudioClip changePageClip;
    [SerializeField]
    private AudioClip selectionClip;
    [SerializeField]
    private List<GameObject> pages;
    [SerializeField]
    private List<GameObject> page22;
    [SerializeField]
    private GameObject quitPage;
    [SerializeField]
    private GameObject background;
    [SerializeField]
    private ToggleSettings[] settings;
    [SerializeField]
    private Image soundControlImage;
    [SerializeField]
    private Sprite[] soundControlSprites;

    private bool fromMenu = false;
    private int index = 0;
    private float startVolume;

    private AudioSource audioSource;
    private Animator soundControlAnimator;
    private int soundControlSOUNDParameter = Animator.StringToHash("Sound");
    private int soundControlNOSOUNDParameter = Animator.StringToHash("No Sound");

    #endregion

    #region [ METHODS ]

    /// <summary>
    /// Start is called on the frame when a script is enabled just before any of the Update methods is called the first time.
    /// </summary>
    void Start()
    {
        audioSource = GetComponent<AudioSource>();

        startVolume = audioSource.volume;

        for (int i = 0; i < settings.Length; i++)
        {
            settings[i].Initialization();
        }

        GlobalVariables.Menu.gameObject.SetActive(false);

        quitPage.SetActive(false);
        page22.ForEach(p => p.SetActive(false));

        if (pages.Count == 0)
        {
            Debug.LogError("Não há nenhuma página!");
        }

        pages.ForEach(p => p.SetActive(false));
        pages[index].SetActive(true);

        if (soundControlImage)
        {
            soundControlAnimator = soundControlImage.GetComponent<Animator>();
        }
    }

    /// <summary>
    /// Update is called every frame, if the MonoBehaviour is enabled.
    /// </summary>
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            ShowQuitPage();
        }

        TestDoubleTap();
    }

    void OnGUI()
    {
#if UNITY_EDITOR
		Event e = Event.current;
		if (e.isMouse && e.type == EventType.MouseDown && e.clickCount == 2) {
			PlayOrPausePlaybackSound(VoiceAction.Reverse);
		}
#endif
    }

    /// <summary>
    /// Doubles the tap.
    /// </summary>
    private void TestDoubleTap()
    {
#if !UNITY_EDITOR
        for (var i = 0; i < Input.touchCount; ++i)
        {
            if (Input.GetTouch(i).phase == TouchPhase.Began)
            {
                if (Input.GetTouch(i).tapCount == 2)
                {
                    PlayOrPausePlaybackSound(VoiceAction.Reverse);
                }
            }
        }
#endif
    }

    /// <summary>
    /// Plaies the or stop soundtrack.
    /// </summary>
    public void PlayOrStopSoundtrack()
    {
        if (GlobalVariables.EnableSoundtrack && !audioSource.isPlaying)
        {
            audioSource.Play();
        }
        else if (!GlobalVariables.EnableSoundtrack && audioSource.isPlaying)
        {
            audioSource.Stop();
        }
    }

    /// <summary>
    /// Pauses the playback.
    /// </summary>
    public void PlayOrPausePlaybackSound(VoiceAction action)
    {
        AudioSource[] audios = pages[index].GetComponentsInChildren<AudioSource>();

        var voice = audios.SingleOrDefault(a => a.CompareTag("Audio"));

        if (!ReferenceEquals(voice, null))
        {
            if (action == VoiceAction.Reverse)
            {
                GlobalVariables.EnablePlaybackSound = !GlobalVariables.EnablePlaybackSound;
            }
            else if (action == VoiceAction.Play)
            {
                GlobalVariables.EnablePlaybackSound = true;
            }
            else if (action == VoiceAction.Pause)
            {
                GlobalVariables.EnablePlaybackSound = false;
            }

            if (voice.isPlaying && !GlobalVariables.EnablePlaybackSound)
            {
                soundControlImage.sprite = soundControlSprites[(int)SoundControlIcon.Pause];
                soundControlAnimator.SetTrigger(soundControlNOSOUNDParameter);

                voice.Pause();
            }
            else if (!voice.isPlaying && GlobalVariables.EnablePlaybackSound)
            {
                soundControlImage.sprite = soundControlSprites[(int)SoundControlIcon.Play];
                soundControlAnimator.SetTrigger(soundControlSOUNDParameter);

                voice.Play();
            }
        }
    }

    /// <summary>
    /// Shows the quit page.
    /// </summary>
    public void ShowQuitPage()
    {
        GlobalVariables.Menu.Close();

        if (quitPage.activeInHierarchy)
        {
            quitPage.SetActive(false);
        }
        else
        {
            quitPage.SetActive(true);
        }
    }

    /// <summary>
    /// Nexts the page by menu.
    /// </summary>
    /// <param name="nextIndex">Next index.</param>
    public void NextPageByMenu(int nextIndex)
    {
        fromMenu = true;

        NextPage(nextIndex);

        fromMenu = false;
    }

    /// <summary>
    /// Plaies the pagination clip.
    /// </summary>
    public void PlayPaginationClip()
    {
        if (GlobalVariables.EnableRingingSounds)
        {
            AudioSource.PlayClipAtPoint(changePageClip, Vector3.zero);
        }
    }

    public void NextPage(int nextIndex)
    {
        // MENU (if open, to close).
        if (GlobalVariables.Menu.gameObject.activeInHierarchy)
        {
            GlobalVariables.Menu.Close();
        }
        // INDICE (if open, to close).
        if (GlobalVariables.Indice.gameObject.activeInHierarchy)
        {
            GlobalVariables.Indice.Close();
        }

        // ENVIRONMENT MUSIC.
        if (nextIndex >= (int)BookPageType.Patricio_Blau_Cover)
        {
            audioSource.DOFade(0, 0.5f).OnComplete(() => audioSource.Stop());
        }
        else if (GlobalVariables.EnableSoundtrack && !audioSource.isPlaying)
        {
            audioSource.DOFade(startVolume, 0.5f).OnStart(() =>
            {
                audioSource.volume = 0f;
                audioSource.Play();
            });
        }

        // SFX PAGINATION.
        BookPageType currentPage = (BookPageType)index;

        if (GlobalVariables.EnableRingingSounds)
        {
            if (!fromMenu && currentPage != BookPageType.BookCover)
            {
                PlayPaginationClip();
            }
            else
            {
                AudioSource.PlayClipAtPoint(selectionClip, Vector3.zero);
            }
        }

        AudioSource[] audiosToStop = pages[index].GetComponentsInChildren<AudioSource>();
        audiosToStop.ToList().ForEach(a => a.Stop());

        int lastIndex = index;
        index = nextIndex;

        float x = Screen.width;
        if (index > lastIndex)
        {
            x *= -1;
        }

        const string TextPageNumberName = "Text Page (number)";
        float duration = 0.5f;

        var lastTextNumberChild = pages[lastIndex].transform.FindChild(TextPageNumberName);
        var currentTextNumberChild = pages[index].transform.FindChild(TextPageNumberName);

        if (lastTextNumberChild && currentTextNumberChild)
        {
            if (lastTextNumberChild.GetComponent<Text>().text == currentTextNumberChild.GetComponent<Text>().text)
            {
                duration = 0f;
            }
        }

        pages[lastIndex].transform.DOLocalMoveX(x * 2, duration).OnComplete(() => pages[lastIndex].SetActive(false));
        pages[index].transform.DOLocalMoveX(-x * 2, 0);
        pages[index].transform.DOLocalMoveX(0, duration).OnStart(() => pages[index].SetActive(true)).OnComplete(() =>
        {
            ExecuteNextPage();
        });
    }

    void ExecuteNextPage()
    {
        // PAGE 22: ANIMATIONS.
        BookPageType nextPage = (BookPageType)index;

        if (nextPage == BookPageType.Melancia_CocoVerde_Page_22_1)
        {
            page22[0].SetActive(true);
            page22[1].SetActive(false);

            background.SetActive(false);
        }
        else if (nextPage == BookPageType.Melancia_CocoVerde_Page_22_2)
        {
            page22[1].SetActive(true);
            page22[0].SetActive(false);

            background.SetActive(false);
        }
        else
        {
            page22.ForEach(p => p.SetActive(false));
            background.SetActive(true);
        }

        // AUDIO.
        AudioSource[] audiosToPlay = pages[index].GetComponentsInChildren<AudioSource>();

        if (GlobalVariables.EnableSoundEffects)
        {
            audiosToPlay.Where(a => a.CompareTag("SFX")).ToList().ForEach(a =>
            {
                if (!a.isPlaying)
                {
                    a.DOFade(1f, 0.5f).OnStart(() =>
                    {
                        a.volume = 0f;
                        a.Play();
                    });
                }
            });
        }
        if (GlobalVariables.EnablePlaybackSound)
        {
            audiosToPlay.Where(a => a.CompareTag("Audio")).ToList().ForEach(a =>
            {
                if (!a.isPlaying)
                {
                    a.DOFade(1f, 0.5f).OnStart(() =>
                    {
                        a.volume = 0f;
                        a.Play();
                    });
                }
            });
        }

        // MENU.
        if (index > 0 && !GlobalVariables.Menu.gameObject.activeInHierarchy)
        {
            GlobalVariables.Menu.gameObject.SetActive(true);
        }
        else if (index == 0 && GlobalVariables.Menu.gameObject.activeInHierarchy)
        {
            GlobalVariables.Menu.gameObject.SetActive(false);
        }
    }

    public void OpenURL(string url)
    {
        Application.OpenURL(url);
    }

    #endregion
}