﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class ToggleSettings : MonoBehaviour
{
	#region [ FIELDS ]

	[SerializeField]
	private TypeSetting typeSetting;

	[SerializeField]
	private AudioClip selectionClip;

	[SerializeField]
	private Image checkedToggleImage = null, uncheckedToggleImage = null;

	[SerializeField]
	private Sprite[] checkedToggleSprites = new Sprite[2];
	[SerializeField]
	private Sprite[] uncheckedToggleSprites = new Sprite[2];

	#endregion

	#region [ METHODS ]

	/// <summary>
	/// Start is called on the frame when a script is enabled just before any of the Update methods is called the first time.
	/// </summary>
	public void Initialization()
	{
		LoadPrefs();
	}

	/// <summary>
	/// Saves the prefs.
	/// </summary>
	private void SavePrefs(bool isEnabled)
	{
		PlayerPrefs.SetInt(typeSetting.ToString(), Convert.ToInt16(isEnabled));
	}

	/// <summary>
	/// Loads the prefs.
	/// </summary>
	private void LoadPrefs()
	{
		bool isEnabled = Convert.ToBoolean(PlayerPrefs.GetInt(typeSetting.ToString(), 1));

		SwitchToggle(isEnabled);
	}

	/// <summary>
	/// Changes the toggle.
	/// </summary>
	public void ChangeToggle()
	{
		if (GlobalVariables.EnableRingingSounds) {
			AudioSource.PlayClipAtPoint(selectionClip, Vector3.zero);
		}

		bool isEnabled = false;

		switch (typeSetting) {
			case TypeSetting.RingingSounds:
				{
					GlobalVariables.EnableRingingSounds = !GlobalVariables.EnableRingingSounds;
					isEnabled = GlobalVariables.EnableRingingSounds;

					break;
				}
			case TypeSetting.PlaybackSound:
				{
					GlobalVariables.EnablePlaybackSound = !GlobalVariables.EnablePlaybackSound;
					isEnabled = GlobalVariables.EnablePlaybackSound;

					break;
				}
			case TypeSetting.SoundEffects:
				{
					GlobalVariables.EnableSoundEffects = !GlobalVariables.EnableSoundEffects;
					isEnabled = GlobalVariables.EnableSoundEffects;
				
					break;
				}
			case TypeSetting.Soundtrack:
				{
					GlobalVariables.EnableSoundtrack = !GlobalVariables.EnableSoundtrack;
					isEnabled = GlobalVariables.EnableSoundtrack;

					GlobalVariables.Manager.PlayOrStopSoundtrack();

					break;
				}
		}

		SwitchImages(isEnabled);

		SavePrefs(isEnabled);
	}

	private void SwitchImages(bool isEnabled)
	{
		if (isEnabled) {
			checkedToggleImage.sprite = checkedToggleSprites[0];
			uncheckedToggleImage.sprite = uncheckedToggleSprites[1];
		}
		else {
			checkedToggleImage.sprite = checkedToggleSprites[1];
			uncheckedToggleImage.sprite = uncheckedToggleSprites[0];
		}
	}

	private void SwitchToggle(bool isEnabled)
	{
		switch (typeSetting) {
			case TypeSetting.RingingSounds:
				{
					GlobalVariables.EnableRingingSounds = isEnabled;
				
					break;
				}
			case TypeSetting.PlaybackSound:
				{
					GlobalVariables.EnablePlaybackSound = isEnabled;
				
					break;
				}
			case TypeSetting.SoundEffects:
				{
					GlobalVariables.EnableSoundEffects = isEnabled;
				
					break;
				}
			case TypeSetting.Soundtrack:
				{
					GlobalVariables.EnableSoundtrack = isEnabled;

					GlobalVariables.Manager.PlayOrStopSoundtrack();

					break;
				}
		}
		
		SwitchImages(isEnabled);
	}

	#endregion
}